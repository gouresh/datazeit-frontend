import { Chart } from '@antv/g2';
import { io } from "socket.io-client";
let url = 'http://135.181.102.185:3000/';


let data = [];

const socket = io('http://135.181.102.185:4001/');

socket.on('connect', function () {
   socket.emit('room', 'queue-data');
  console.log("socket connected");
});

socket.on('message', function (data) {
  //in data there will be event on which we will call the respective function
  console.log(data);
  chart.data(data);
  console.log(data);
  chart.render()

});

const chart = new Chart({
  container: 'container',
  autoFit: true,
  height: 500,
});

chart.coordinate('theta', {
  radius: 0.75,
});



chart.scale('percent', {
  formatter: (val) => {
    val = val + '%';
    return val;
  },
});

chart.tooltip({
  showTitle: false,
  showMarkers: false,
});

chart
  .interval()
  .position('percent')
  .color('status')
  .label('percent', {
    content: (data) => {
      return `${data.status}: ${data.percent}%`;
    },
  })
  .adjust('stack');

chart.interaction('element-active');
var api = {
  link: url+'jobs/count',
  object: {
    method: 'GET',
    headers: {
      'API_KEY': 'MATCHINGDATAFASTERTHANTINDERMATCHINGPEOPLE',
    }
  }
}

fetch(api.link,api.object)
      .then((res) => res.json())
      .then((json)=>{
        data=json;
        chart.data(data);
        console.log(data);
        chart.render()})
      

